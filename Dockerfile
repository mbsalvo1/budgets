# ---- Build Stage ----
FROM node:14 AS build-stage

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

RUN yarn build

# ---- Production Stage ----
FROM node:14

WORKDIR /app

COPY --from=build-stage /app/build /app/build
COPY package.json yarn.lock ./

RUN yarn install --production

# FastAPI setup
RUN apt-get update && \
    apt-get install -y python3 python3-pip

COPY backend/requirements.txt /app/backend/
RUN pip3 install -r /app/backend/requirements.txt

COPY backend /app/backend

EXPOSE 3000
EXPOSE 8000

CMD ["sh", "-c", "yarn start & uvicorn backend.main:app --host 0.0.0.0 --port 8000"]
