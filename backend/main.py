from fastapi import FastAPI
from motor.motor_asyncio import AsyncIOMotorClient
from pydantic import BaseModel

app = FastAPI()

class MongoDB:
    client: AsyncIOMotorClient = None

class Item(BaseModel):
    name: str
    description: str
    price: float

@app.on_event("startup")
async def startup_db_client():
    MongoDB.client = AsyncIOMotorClient("mongodb://db:27017")
    app.mongodb = MongoDB.client.my_database

@app.on_event("shutdown")
async def shutdown_db_client():
    MongoDB.client.close()

@app.post("/items/")
async def create_item(item: Item):
    result = await app.mongodb["items"].insert_one(item.dict())
    return {"_id": str(result.inserted_id)}

@app.get("/items/")
async def get_items():
    items = []
    async for item in app.mongodb["items"].find():
        item["_id"] = str(item["_id"])
        items.append(item)
    return items
