import React, { useState } from "react";
import { Button, Typography, Container, Modal } from "@mui/material";
import { styled } from "@mui/system";
import KeyboardArrowDownRoundedIcon from "@mui/icons-material/KeyboardArrowDownRounded";

const StyledContainer = styled(Container)(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  minHeight: "100vh",
  textAlign: "center",
}));

const BouncingButton = styled(Button)(({ theme }) => ({
  marginTop: 16,
  animation: "$bounce 2s infinite",
  "@keyframes bounce": {
    "0%, 20%, 50%, 80%, 100%": {
      transform: "translateY(0)",
    },
    "40%": {
      transform: "translateY(-20px)",
    },
    "60%": {
      transform: "translateY(-10px)",
    },
  },
}));

function HomePage() {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <StyledContainer>
      <Typography variant="h2" component="h1" gutterBottom>
        Welcome to your personal Budget Creator
      </Typography>
      <BouncingButton
        variant="contained"
        color="primary"
        size="large"
        onClick={handleOpen}
        endIcon={<KeyboardArrowDownRoundedIcon />}
      >
        Start Here
      </BouncingButton>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="start-modal"
        aria-describedby="welcome-to-budget-creator"
      >
        <div style={{ padding: 24, backgroundColor: "white", borderRadius: 8 }}>
          <Typography variant="h4" component="h2" gutterBottom>
            Let's get started
          </Typography>
          {/* Add your form or content here */}
        </div>
      </Modal>
    </StyledContainer>
  );
}

export default HomePage;
